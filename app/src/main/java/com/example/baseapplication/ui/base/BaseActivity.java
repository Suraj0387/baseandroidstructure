package com.example.baseapplication.ui.base;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.RequestManager;
import com.example.baseapplication.di.component.DaggerViewModelComponent;
import com.example.baseapplication.di.component.ViewModelComponent;
import com.example.baseapplication.di.modules.ActivityModule;
import com.example.baseapplication.factory.ViewModelFactory;

import javax.inject.Inject;

public abstract class BaseActivity<T extends ViewDataBinding,V extends ViewModel> extends AppCompatActivity {

    @Inject
    ViewModelFactory mViewModelFactory;
    @Inject
    ProgressDialog mProgressDialog;
    @Inject
    AlertDialog alertDialog;

    private T mViewDataBinding;
    protected V mViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        performDependencyInjection(getBuildComponent());
        createViewModel();
        performDataBinding();


    }

    private void performDataBinding() {
        mViewDataBinding = DataBindingUtil.setContentView(this, getLayoutId());
        mViewDataBinding.setVariable(getBindingVariable(), mViewModel);
        mViewDataBinding.executePendingBindings();
        mViewDataBinding.setLifecycleOwner(this);
    }

    private ViewModelComponent getBuildComponent() {

        return DaggerViewModelComponent.builder()
                .appComponent(((BaseApp)getApplication()).getAppComponent())
                .activityModule(new ActivityModule(this)).build();
    }

    private void createViewModel()
    {
        mViewModel= (V) new ViewModelProvider(this, mViewModelFactory).get(getViewModelName());

    }

    public void showLoading() {
        hideLoading();
        mProgressDialog.show();
        if (mProgressDialog.getWindow() != null) {
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

    }

    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public void loadImage(ImageView imageView)
    {
//        requestManager.load(icon)
//                .into(imageView);
    }

    public V getmViewModel() {
        return mViewModel;
    }

    public void showToastMessage(String message)
    {
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }

    public abstract
    @LayoutRes
    int getLayoutId();
    public abstract void performDependencyInjection(ViewModelComponent buildComponent);
    public abstract Class<V> getViewModelName();
    public abstract int getBindingVariable();



}