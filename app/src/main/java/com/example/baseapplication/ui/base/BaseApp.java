package com.example.baseapplication.ui.base;

import android.app.Application;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.example.baseapplication.di.component.AppComponent;
import com.example.baseapplication.di.component.DaggerAppComponent;
import com.example.baseapplication.di.modules.AppModule;

import javax.inject.Inject;

public class BaseApp  extends Application {


    private AppComponent myAppComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();

    }

    private void initAppComponent(){
            myAppComponent= DaggerAppComponent.builder().appModule(new AppModule(this))
                .build();

    }
    public AppComponent getAppComponent() {
        return myAppComponent;
    }


}
