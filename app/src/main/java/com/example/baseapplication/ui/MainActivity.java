package com.example.baseapplication.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.example.baseapplication.BR;
import com.example.baseapplication.R;
import com.example.baseapplication.databinding.ActivityMainBinding;
import com.example.baseapplication.di.component.ViewModelComponent;
import com.example.baseapplication.ui.base.BaseActivity;
import com.example.baseapplication.ui.base.BaseApp;
import com.example.baseapplication.viewmodel.LoginViewModel;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding,LoginViewModel> {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //((Button) findViewById(R.id.btn_Submit)).setOnClickListener(v-> mViewModel.getLogin());
        final Observer<Boolean> resultObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable final Boolean isProgressEnabled) {
                if(isProgressEnabled)
                    showLoading();
                else
                    hideLoading();
            }
        };

        getmViewModel().getResult().observe(this, resultObserver);

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void performDependencyInjection(ViewModelComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public Class<LoginViewModel>  getViewModelName() {
        return LoginViewModel.class;
    }

    @Override
    public int getBindingVariable() {
        return BR.loginViewModel;
    }
}