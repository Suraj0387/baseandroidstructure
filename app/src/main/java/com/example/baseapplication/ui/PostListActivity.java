package com.example.baseapplication.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.baseapplication.BR;
import com.example.baseapplication.R;
import com.example.baseapplication.databinding.ActivityPostListBinding;
import com.example.baseapplication.databinding.ListItemPostBinding;
import com.example.baseapplication.di.component.ViewModelComponent;
import com.example.baseapplication.model.Post;
import com.example.baseapplication.ui.base.BaseAdapter;
import com.example.baseapplication.ui.base.BaseRecyclerViewActivity;
import com.example.baseapplication.viewmodel.PostListViewModel;

public class PostListActivity extends BaseRecyclerViewActivity<ActivityPostListBinding, PostListViewModel,RecyclerView.Adapter> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpRecyclerView(false);
        getmViewModel().getPostsData();
        final Observer<Boolean> resultObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable final Boolean isProgressEnabled) {
                if(isProgressEnabled)
                    showLoading();
                else
                    //hide the loader
                    hideLoading();
            }
        };
        final Observer<String> toastMessage = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String message) {
                showToastMessage(message);
            }
        };

        getmViewModel().getTestMessage().observe(this,toastMessage);
        getmViewModel().getResult().observe(this, resultObserver);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_post_list;
    }


    @Override
    public void performDependencyInjection(ViewModelComponent buildComponent) {
        buildComponent.inject(this);
    }

    @Override
    public Class<PostListViewModel> getViewModelName() {
        return PostListViewModel.class;
    }

    @Override
    public int getBindingVariable() {
        return BR.postListViewModel;
    }

    @Override
    protected void onClickViewItem(View view, int position, float x, float y) {

    }

    @Override
    protected RecyclerView.Adapter getRecyclerAdapter() {
        return new BaseAdapter<Post, ListItemPostBinding>(this, getmViewModel().getPosts().getValue()) {
            @Override
            public int getAdapterLayoutResId() {
                return R.layout.list_item_post;
            }

            @Override
            public void onBindData(Post model, int position, ListItemPostBinding dataBinding) {
                dataBinding.setPost(model);
            }

            @Override
            public void onItemClick(Post model, int position) {

            }
        };
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(this);
    }

    @Override
    protected RecyclerView.OnScrollListener getScrollListener() {
        return null;
    }

    @Override
    protected int getLayoutView() {
        return R.id.rv_posts;
    }
}