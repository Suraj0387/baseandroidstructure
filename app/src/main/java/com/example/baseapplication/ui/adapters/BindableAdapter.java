package com.example.baseapplication.ui.adapters;

public interface BindableAdapter<T> {
   public void setData(T data);
}