package com.example.baseapplication.ui.base;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;

import com.example.baseapplication.R;

public abstract class BaseRecyclerViewActivity<T extends ViewDataBinding,V extends ViewModel,A extends RecyclerView.Adapter> extends BaseActivity<T,V > {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void setUpRecyclerView(boolean isNestingScroll)
    {
        recyclerView = (RecyclerView)findViewById(getLayoutView());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setAdapter(getRecyclerAdapter());
    }

    protected void setPagination()
    {
        recyclerView.addOnScrollListener(getScrollListener());
    }



    protected abstract void onClickViewItem(View view, int position, float x, float y);
    protected abstract A getRecyclerAdapter();
    protected abstract RecyclerView.LayoutManager getLayoutManager();
    protected abstract  RecyclerView.OnScrollListener getScrollListener();
    protected abstract int getLayoutView();
}
