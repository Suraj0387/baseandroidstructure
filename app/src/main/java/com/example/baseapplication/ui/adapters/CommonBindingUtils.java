package com.example.baseapplication.ui.adapters;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class CommonBindingUtils {
    @BindingAdapter("bindItems")
    public static <T> void setItems(RecyclerView recyclerView, T items) {
        if (recyclerView.getAdapter() instanceof  BindableAdapter<?>) {
            ((BindableAdapter<T> )recyclerView.getAdapter() ).setData(items);
        }
    }
}