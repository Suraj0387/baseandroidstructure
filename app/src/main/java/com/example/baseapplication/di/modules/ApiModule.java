package com.example.baseapplication.di.modules;

import android.app.Application;

import com.example.baseapplication.api.ApiEndPoint;
import com.example.baseapplication.utils.NetworkConnectionInterceptor;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    private String baseUrl ="http://jsonplaceholder.typicode.com/";

    @Singleton
    @Provides
    public  Retrofit getRetrofitInstance(Gson gson, OkHttpClient okHttpClient)
    {
        return new Retrofit.Builder()
                           .baseUrl(baseUrl)
                           .addConverterFactory(GsonConverterFactory.create(gson))
                           .client(okHttpClient)
                           .build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache,HttpLoggingInterceptor interceptor,NetworkConnectionInterceptor networkInterceptor) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(interceptor);
        client.addInterceptor(networkInterceptor);
        client.cache(cache);
        return client.build();
    }

    @Singleton
    @Provides
    ApiEndPoint providesApiEndPoint(Retrofit retrofit)
    {
        return retrofit.create(ApiEndPoint.class);
    }


    @Provides
    @Singleton
    Cache provideOkHttpCache(Application application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideHttpLogginIntercepter() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        return logging;
    }

    @Provides
    @Singleton
    NetworkConnectionInterceptor providesNetworkConnectionInterceptor(Application application)
    {
        return new NetworkConnectionInterceptor(application);
    }

}
