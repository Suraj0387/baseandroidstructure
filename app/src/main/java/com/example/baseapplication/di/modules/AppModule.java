package com.example.baseapplication.di.modules;

import android.app.Application;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.example.baseapplication.R;
import com.example.baseapplication.utils.NetworkConnectionInterceptor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    RequestOptions provideRequestOptions() {
        return RequestOptions.placeholderOf(R.mipmap.ic_launcher).error(R.mipmap.ic_launcher);
    }

    @Provides
    @Singleton
    RequestManager provideGlideInstance(Application application, RequestOptions requestOptions) {
        return Glide.with(application).setDefaultRequestOptions(requestOptions);
    }

    @Provides
    Drawable provideAppDrawable(Application application) {
        return ContextCompat.getDrawable(mApplication, R.mipmap.ic_launcher);
    }



}
