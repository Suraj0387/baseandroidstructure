package com.example.baseapplication.di.component;

import com.example.baseapplication.di.modules.ActivityModule;
import com.example.baseapplication.di.modules.ApiModule;
import com.example.baseapplication.ui.MainActivity;
import com.example.baseapplication.di.modules.ViewModelFactoryModule;
import com.example.baseapplication.di.modules.ViewModelModule;
import com.example.baseapplication.ui.PostListActivity;

import javax.inject.Singleton;

import dagger.Component;
@ActivityScope
@Component(modules = {ViewModelModule.class, ActivityModule.class}, dependencies = AppComponent.class)
public interface ViewModelComponent {
    void inject(MainActivity mainActivity);
    void inject(PostListActivity postListActivity);

}