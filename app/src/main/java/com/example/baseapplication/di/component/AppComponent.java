package com.example.baseapplication.di.component;

import android.app.Application;

import com.example.baseapplication.api.ApiEndPoint;
import com.example.baseapplication.di.modules.ApiModule;
import com.example.baseapplication.di.modules.AppModule;
import com.example.baseapplication.di.modules.ViewModelFactoryModule;
import com.example.baseapplication.ui.base.BaseApp;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class,ApiModule.class, ViewModelFactoryModule.class})
public interface AppComponent {
    ApiEndPoint getApiEndPoint();

}
