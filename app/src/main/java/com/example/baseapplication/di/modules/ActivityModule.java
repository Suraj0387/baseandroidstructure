package com.example.baseapplication.di.modules;

import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.example.baseapplication.R;
import com.example.baseapplication.ui.base.BaseActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    BaseActivity<?,?> mActivity;

    public ActivityModule(BaseActivity activity)
    {
        mActivity=activity;
    }

    @Provides
    ProgressDialog providesProgressDialog() {
        return  new ProgressDialog(mActivity);
    }

    @Provides
    AlertDialog providesAlertDialog() {
        return new AlertDialog.Builder(mActivity).create();
    }
}
