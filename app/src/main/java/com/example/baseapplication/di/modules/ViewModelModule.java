package com.example.baseapplication.di.modules;

import androidx.lifecycle.ViewModel;

import com.example.baseapplication.AppDataManager;
import com.example.baseapplication.DataManager;
import com.example.baseapplication.di.annotations.ViewModelKey;
import com.example.baseapplication.viewmodel.LoginViewModel;
import com.example.baseapplication.viewmodel.PostListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    ViewModel bindLoginViewModel(LoginViewModel loginViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PostListViewModel.class)
    ViewModel bindPostListViewModel(PostListViewModel postListViewModel);

    @Binds
    DataManager bindDataManager(AppDataManager implementer);
}
