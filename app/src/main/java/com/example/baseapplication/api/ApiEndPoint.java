package com.example.baseapplication.api;

import com.example.baseapplication.model.LoginRequest;
import com.example.baseapplication.model.LoginResponse;
import com.example.baseapplication.model.Post;
import com.example.baseapplication.model.PostList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiEndPoint {

    @POST("/posts")
    Call<LoginResponse> getLogin(@Body LoginRequest requestObject);

    @GET("/posts")
    Call<ArrayList<Post>> getPosts();

}