package com.example.baseapplication.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.baseapplication.DataManager;
import com.example.baseapplication.model.DeviceInfo;
import com.example.baseapplication.model.LoginRequest;
import com.example.baseapplication.model.LoginResponse;
import com.example.baseapplication.model.Post;
import com.example.baseapplication.model.PostList;
import com.example.baseapplication.model.SecParam;
import com.example.baseapplication.utils.NoConnectivityException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostListViewModel extends ViewModel {

    DataManager mDataManager;
    private MutableLiveData<Boolean> progressDialog = new MutableLiveData<>();
    public MutableLiveData<ArrayList<Post>> posts = new MutableLiveData<>();
    public MutableLiveData<String> tostMessage = new MutableLiveData<>();

    @Inject
    public PostListViewModel(DataManager manager) {
        mDataManager=manager;
    }

    public void getPostsData()
    {
        progressDialog.setValue(true);

        mDataManager.getPosts().enqueue(new Callback<ArrayList<Post>>() {
                        @Override
                        public void onResponse(Call<ArrayList<Post>> call, Response<ArrayList<Post>> response) {
                            progressDialog.setValue(false);

                            posts.setValue(response.body());

                        }

                        @Override
                        public void onFailure(Call<ArrayList<Post>> call, Throwable t) {
                            // Log error here since request failed
                            progressDialog.setValue(false);
                            if(t instanceof NoConnectivityException) {
                                tostMessage.setValue(t.getMessage());
                            }

                        }
                    });
    }

    public LiveData<Boolean> getResult()
    {
        return progressDialog;
    }

    public LiveData<ArrayList<Post>> getPosts()
    {
        return posts;
    }
    public LiveData<String> getTestMessage()
    {
        return tostMessage;
    }
}
