package com.example.baseapplication.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.baseapplication.DataManager;
import com.example.baseapplication.model.DeviceInfo;
import com.example.baseapplication.model.LoginRequest;
import com.example.baseapplication.model.LoginResponse;
import com.example.baseapplication.model.SecParam;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    DataManager mDataManager;
    private MutableLiveData<Boolean> progressDialog = new MutableLiveData<>();
    public MutableLiveData<String> status = new MutableLiveData<>();

    @Inject
    public LoginViewModel(DataManager manager) {
        mDataManager=manager;
    }

    public void getLogin()
    {
        progressDialog.setValue(true);
        LoginRequest request = new LoginRequest();
        SecParam secparam = new SecParam();
        secparam.setUserName("7972508445");
        secparam.setPwd("i5tzrkg1C3P03XY1dro8DQ==");
        DeviceInfo info = new DeviceInfo();
        request.setSecParam(secparam);
        request.setDeviceInfo(info);

        mDataManager.getLogin(request).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            progressDialog.setValue(false);
                            status.setValue("success");

                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            // Log error here since request failed
                            System.out.println("Error");
                            status.setValue("fail");
                        }
                    });
    }

    public LiveData<Boolean> getResult()
    {
        return progressDialog;
    }
}
