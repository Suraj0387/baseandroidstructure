
package com.example.baseapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ComResp implements Parcelable {


    private List<String> mAddiParams;

    private ChMessage mChMessage;

    private String mReqSecureKey;

    private String mUrlDecision;

    public List<String> getAddiParams() {
        return mAddiParams;
    }

    public void setAddiParams(List<String> addiParams) {
        mAddiParams = addiParams;
    }

    public ChMessage getChMessage() {
        return mChMessage;
    }

    public void setChMessage(ChMessage chMessage) {
        mChMessage = chMessage;
    }

    public String getReqSecureKey() {
        return mReqSecureKey;
    }

    public void setReqSecureKey(String reqSecureKey) {
        mReqSecureKey = reqSecureKey;
    }

    public String getUrlDecision() {
        return mUrlDecision;
    }

    public void setUrlDecision(String urlDecision) {
        mUrlDecision = urlDecision;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.mAddiParams);
        dest.writeParcelable(this.mChMessage, flags);
        dest.writeString(this.mReqSecureKey);
        dest.writeString(this.mUrlDecision);
    }

    public void readFromParcel(Parcel source) {
        this.mAddiParams = source.createStringArrayList();
        this.mChMessage = source.readParcelable(ChMessage.class.getClassLoader());
        this.mReqSecureKey = source.readString();
        this.mUrlDecision = source.readParcelable(String.class.getClassLoader());
    }

    public ComResp() {
    }

    protected ComResp(Parcel in) {
        this.mAddiParams = in.createStringArrayList();
        this.mChMessage = in.readParcelable(ChMessage.class.getClassLoader());
        this.mReqSecureKey = in.readString();
        this.mUrlDecision = in.readParcelable(String.class.getClassLoader());
    }

    public static final Parcelable.Creator<ComResp> CREATOR = new Parcelable.Creator<ComResp>() {
        @Override
        public ComResp createFromParcel(Parcel source) {
            return new ComResp(source);
        }

        @Override
        public ComResp[] newArray(int size) {
            return new ComResp[size];
        }
    };
}
