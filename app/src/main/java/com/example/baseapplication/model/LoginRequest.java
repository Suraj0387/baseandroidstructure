
package com.example.baseapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LoginRequest implements Parcelable {


    private DeviceInfo mDeviceInfo;

    private SecParam mSecParam;

    public DeviceInfo getDeviceInfo() {
        return mDeviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        mDeviceInfo = deviceInfo;
    }

    public SecParam getSecParam() {
        return mSecParam;
    }

    public void setSecParam(SecParam secParam) {
        mSecParam = secParam;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mDeviceInfo, flags);
        dest.writeParcelable(this.mSecParam, flags);
    }

    public void readFromParcel(Parcel source) {
        this.mDeviceInfo = source.readParcelable(DeviceInfo.class.getClassLoader());
        this.mSecParam = source.readParcelable(SecParam.class.getClassLoader());
    }

    public LoginRequest() {
    }

    protected LoginRequest(Parcel in) {
        this.mDeviceInfo = in.readParcelable(DeviceInfo.class.getClassLoader());
        this.mSecParam = in.readParcelable(SecParam.class.getClassLoader());
    }

    public static final Parcelable.Creator<LoginRequest> CREATOR = new Parcelable.Creator<LoginRequest>() {
        @Override
        public LoginRequest createFromParcel(Parcel source) {
            return new LoginRequest(source);
        }

        @Override
        public LoginRequest[] newArray(int size) {
            return new LoginRequest[size];
        }
    };
}
