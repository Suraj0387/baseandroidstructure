
package com.example.baseapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SecParam implements Parcelable {

    private String mPwd;
    private String mUserName;

    public String getPwd() {
        return mPwd;
    }

    public void setPwd(String pwd) {
        mPwd = pwd;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mPwd);
        dest.writeString(this.mUserName);
    }

    public void readFromParcel(Parcel source) {
        this.mPwd = source.readString();
        this.mUserName = source.readString();
    }

    public SecParam() {
    }

    protected SecParam(Parcel in) {
        this.mPwd = in.readString();
        this.mUserName = in.readString();
    }

    public static final Parcelable.Creator<SecParam> CREATOR = new Parcelable.Creator<SecParam>() {
        @Override
        public SecParam createFromParcel(Parcel source) {
            return new SecParam(source);
        }

        @Override
        public SecParam[] newArray(int size) {
            return new SecParam[size];
        }
    };
}
