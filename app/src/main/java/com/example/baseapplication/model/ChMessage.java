
package com.example.baseapplication.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ChMessage implements Parcelable {

    private String mCode;

    private String mDataArr;

    private String mDispType;

    private Boolean mIsError;

    private String mMessage;

    private String mSubject;

    private String mType;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mCode);
        dest.writeString(this.mDataArr);
        dest.writeString(this.mDispType);
        dest.writeValue(this.mIsError);
        dest.writeString(this.mMessage);
        dest.writeString(this.mSubject);
        dest.writeString(this.mType);
    }

    public void readFromParcel(Parcel source) {
        this.mCode = source.readString();
        this.mDataArr = source.readParcelable(String.class.getClassLoader());
        this.mDispType = source.readString();
        this.mIsError = (Boolean) source.readValue(Boolean.class.getClassLoader());
        this.mMessage = source.readString();
        this.mSubject = source.readString();
        this.mType = source.readString();
    }

    public ChMessage() {
    }

    protected ChMessage(Parcel in) {
        this.mCode = in.readString();
        this.mDataArr = in.readParcelable(String.class.getClassLoader());
        this.mDispType = in.readString();
        this.mIsError = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.mMessage = in.readString();
        this.mSubject = in.readString();
        this.mType = in.readString();
    }

    public static final Parcelable.Creator<ChMessage> CREATOR = new Parcelable.Creator<ChMessage>() {
        @Override
        public ChMessage createFromParcel(Parcel source) {
            return new ChMessage(source);
        }

        @Override
        public ChMessage[] newArray(int size) {
            return new ChMessage[size];
        }
    };
}
