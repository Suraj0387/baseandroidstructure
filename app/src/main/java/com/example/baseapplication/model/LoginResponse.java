
package com.example.baseapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class LoginResponse implements Parcelable {


    private String mAppStatus;

    private ComResp mComResp;

    private String mHKey;
    private String mLastLoggedInDate;
    private List<String> mLoggedInModList;
    private String mOtp;
    private String mSecQuests;
    private String mStatus;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mAppStatus);
        dest.writeParcelable(this.mComResp, flags);
        dest.writeString(this.mHKey);
        dest.writeString(this.mLastLoggedInDate);
        dest.writeList(this.mLoggedInModList);
        dest.writeString(this.mOtp);
        dest.writeString(this.mSecQuests);
        dest.writeString(this.mStatus);
    }

    public void readFromParcel(Parcel source) {
        this.mAppStatus = source.readParcelable(String.class.getClassLoader());
        this.mComResp = source.readParcelable(ComResp.class.getClassLoader());
        this.mHKey = source.readParcelable(String.class.getClassLoader());
        this.mLastLoggedInDate = source.readString();
        this.mLoggedInModList = new ArrayList<String>();
        source.readList(this.mLoggedInModList, String.class.getClassLoader());
        this.mOtp = source.readParcelable(String.class.getClassLoader());
        this.mSecQuests = source.readParcelable(String.class.getClassLoader());
        this.mStatus = source.readString();
    }

    public LoginResponse() {
    }

    protected LoginResponse(Parcel in) {
        this.mAppStatus = in.readParcelable(String.class.getClassLoader());
        this.mComResp = in.readParcelable(ComResp.class.getClassLoader());
        this.mHKey = in.readParcelable(String.class.getClassLoader());
        this.mLastLoggedInDate = in.readString();
        this.mLoggedInModList = new ArrayList<String>();
        in.readList(this.mLoggedInModList, String.class.getClassLoader());
        this.mOtp = in.readParcelable(String.class.getClassLoader());
        this.mSecQuests = in.readParcelable(String.class.getClassLoader());
        this.mStatus = in.readString();
    }

    public static final Parcelable.Creator<LoginResponse> CREATOR = new Parcelable.Creator<LoginResponse>() {
        @Override
        public LoginResponse createFromParcel(Parcel source) {
            return new LoginResponse(source);
        }

        @Override
        public LoginResponse[] newArray(int size) {
            return new LoginResponse[size];
        }
    };
}
