package com.example.baseapplication;

import com.example.baseapplication.api.ApiEndPoint;
import com.example.baseapplication.model.LoginRequest;
import com.example.baseapplication.model.LoginResponse;
import com.example.baseapplication.model.Post;
import com.example.baseapplication.model.PostList;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;

public class AppDataManager implements DataManager{

    private final ApiEndPoint apiEndPoint;
    @Inject
    public AppDataManager(ApiEndPoint endPoint) {
        apiEndPoint=endPoint;
    }

    public Call<LoginResponse> getLogin(LoginRequest request)
    {
     return apiEndPoint.getLogin(request);
    }

    @Override
    public Call<ArrayList<Post>> getPosts() {
        return apiEndPoint.getPosts();
    }

}
