package com.example.baseapplication;

import com.example.baseapplication.model.LoginRequest;
import com.example.baseapplication.model.LoginResponse;
import com.example.baseapplication.model.Post;
import com.example.baseapplication.model.PostList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public interface DataManager {
     Call<LoginResponse> getLogin(LoginRequest request);
     Call<ArrayList<Post>> getPosts();
}
